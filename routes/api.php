<?php

use App\Http\Controllers\Api\{RegisterController, LoginController, TodoController,BiodataController};

Route::post('/register/account', [RegisterController::class, 'store']);
Route::post('/login', [LoginController::class, 'login']);
Route::middleware('auth:api')->namespace('Api')->group(function () {
    Route::get('/me', [LoginController::class, 'me']);
    Route::post('/logout', [LoginController::class, 'logout']);
    Route::get('/todo/index', [TodoController::class, 'index']);
    Route::post('/todo/store', [TodoController::class, 'store']);
    Route::delete('/todo/delete', [TodoController::class, 'destroy']);
    Route::post('/todo/update', [TodoController::class, 'update']);
    Route::prefix('/biodata')->name('biodata.')->group(function () {
        Route::get('/index', [BiodataController::class, 'index']);
        Route::post('/store', [BiodataController::class, 'store']);
        Route::delete('/delete', [BiodataController::class, 'destroy']);
        Route::post('/update', [BiodataController::class, 'update']);
    });
    
});