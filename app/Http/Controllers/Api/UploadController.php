<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UploadResource;
use App\Models\Upload;

class UploadController extends Controller
{
    public function index()
    {
        if (request('search')) {
            $uploads = Upload::where('id','like',"%".request('id')."%")->orWhere('title','like',"%".request('id')."%")->get();
            return response()->json(['data' =>$uploads]);
        }
        if (request('id')) {
            $uploads = Upload::where('id',request('id'))->first();
            return response()->json(['data' =>$uploads]);
        }
        $uploads = Upload::latest()->get();
        return response()->json(['data' =>$uploads]);
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required'
        ]);
        $Upload = auth()->user()->uploads()->create([
            'title' => request('title'),
            'slug' => \Str::slug(request('title').\Str::random(10)),
            'description' => request('description')
        ]);
        return new UploadResource($Upload);
    }

    public function destroy()
    {
        try {
            Upload::where('id',request('id'))->delete();
        } catch (\Exception $e) {
            return response()->json(['error' => "Something went wront".$e]);
        }
        return response()->json(['success' => "Upload deleted successfully!"]);
    }

    public function update()
    {
        if (!request('id')) {
            return response()->json(['error' => "Where Is Id. Pliss Input the id"]);
        }
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required'
        ]);
        Upload::where('id',request('id'))->update([
            'title' => request('title'),
            'description' => request('description')
        ]);
        return response()->json(['success' => "Upload updated successfully!"]);
    }
}
