<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TodoResource;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index()
    {
        if (request('search')) {
            $todos = Todo::where('id','like',"%".request('id')."%")->orWhere('title','like',"%".request('id')."%")->get();
            return response()->json(['data' =>$todos]);
        }
        if (request('id')) {
            $todos = Todo::where('id',request('id'))->first();
            return response()->json(['data' =>$todos]);
        }
        $todos = Todo::latest()->get();
        return response()->json(['data' =>$todos]);
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required'
        ]);
        $todo = auth()->user()->todos()->create([
            'title' => request('title'),
            'slug' => \Str::slug(request('title').\Str::random(10)),
            'description' => request('description')
        ]);
        return new TodoResource($todo);
    }

    public function destroy()
    {
        try {
            Todo::where('id',request('id'))->delete();
        } catch (\Exception $e) {
            return response()->json(['error' => "Something went wront".$e]);
        }
        return response()->json(['success' => "Todo deleted successfully!"]);
    }

    public function update()
    {
        if (!request('id')) {
            return response()->json(['error' => "Where Is Id. Pliss Input the id"]);
        }
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required'
        ]);
        Todo::where('id',request('id'))->update([
            'title' => request('title'),
            'description' => request('description')
        ]);
        return response()->json(['success' => "Todo updated successfully!"]);
    }
}
