<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\BiodataResource;
use App\Models\Biodata;


class BiodataController extends Controller
{
    public function index()
    {
        if (request('search')) {
            $Biodatas = Biodata::where('id','like',"%".request('id')."%")->orWhere('name','like',"%".request('id')."%")->get();
            return response()->json(['data' =>$Biodatas]);
        }
        if (request('id')) {
            $Biodatas = Biodata::where('id',request('id'))->first();
            return response()->json(['data' =>$Biodatas]);
        }
        $Biodatas = Biodata::latest()->get();
        return response()->json(['data' =>$Biodatas]);
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'nohp' => 'required',
            'gender' => 'required',
        ]);
        $Biodata = auth()->user()->Biodatas()->create([
            'name' => request('name'),
            'email' => request('email'),
            'street' => request('street'),
            'city' => request('city'),
            'state' => request('state'),
            'nohp' => request('nohp'),
            'gender' => request('gender'),
        ]);
        return new BiodataResource($Biodata);
    }

    public function destroy()
    {
        try {
            Biodata::where('id',request('id'))->delete();
        } catch (\Exception $e) {
            return response()->json(['error' => "Something went wront".$e]);
        }
        return response()->json(['success' => "Biodata deleted successfully!"]);
    }

    public function update()
    {
        if (!request('id')) {
            return response()->json(['error' => "Where Is Id. Pliss Input the id"]);
        }
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'nohp' => 'required',
            'gender' => 'required',
        ]);
        Biodata::where('id',request('id'))->update([
            'name' => request('name'),
            'email' => request('email'),
            'street' => request('street'),
            'city' => request('city'),
            'state' => request('state'),
            'nohp' => request('nohp'),
            'gender' => request('gender'),
        ]);
        return response()->json(['success' => "Biodata updated successfully!"]);
    }
}
